# For the [React tutorial](https://learn.tylermcginnis.com/courses/50507/)

### Completed website is at [github-battle-5b85b.firebaseapp.com](https://github-battle-5b85b.firebaseapp.com)

[Hosting lesson](https://learn.tylermcginnis.com/courses/50507/lectures/2466854):

![Screenshot of my completed website at Hosting lesson](./screenshots/hosting.png)

[Children lesson](https://learn.tylermcginnis.com/courses/50507/lectures/762576#/questions/1):

![Screenshot of my website at Children lesson](./screenshots/children.png)

[Dynamic rendering & query lesson](https://learn.tylermcginnis.com/courses/50507/lectures/2466820#/questions/1):

![Screenshot of my website at Dynamic Rendering & Query lesson](./screenshots/dynamic.png)

[Forms lesson](https://learn.tylermcginnis.com/courses/50507/lectures/2466819#/questions/1):

![Screenshot of my website at Forms lesson](./screenshots/forms.png)

[Router lesson](https://learn.tylermcginnis.com/courses/50507/lectures/2466764#/questions/1):

![Screenshot of my website at Router lesson](./screenshots/router.png)


[Lifecycle Events lesson](https://learn.tylermcginnis.com/courses/50507/lectures/762618#/questions/1):

![Screenshot of my website at Lifecycle Events lesson](./screenshots/lifecycle.png)